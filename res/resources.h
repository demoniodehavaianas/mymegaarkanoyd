#ifndef _RES_RESOURCES_H_
#define _RES_RESOURCES_H_

extern const SpriteDefinition spBall;
extern const SpriteDefinition spPaddle;
extern const SpriteDefinition spBrickG;
extern const Image bgtile;
extern const Image background;

#endif // _RES_RESOURCES_H_
