#ifndef _RES_SFX_H_
#define _RES_SFX_H_

extern const u8 sfx_ball_hit[6400];
extern const u8 sfx_ball_hit_wall[6912];
extern const u8 sfx_ball_die[12800];
extern const u8 sfx_victory[22784];

#endif // _RES_SFX_H_
