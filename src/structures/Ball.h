/**
 * @file Ball.h
 * @author Nil Obermüller (demoniodehavaianas@outlook.com.br)
 * @brief Sets the Ball info 
 * @version 0.1
 * @date 2022-01-18
 *
 * @copyright Copyright (c) 2022
 */

#ifndef BALL_H
#define BALL_H

/** width in pixels */
const int BALL_WIDTH = 8;
/** height in pixels */
const int BALL_HEIGHT = 8;
/** initial velocity */
const int INITIAL_VELOCITY = 2;

/**
 * @brief Sets Ball's structure using in game
 * \param pos_x
 *      
 * \param pos_y
 * \param vel_x
 * \param vel_y
 * \param collor
 */
typedef struct Ball
{
    int pos_x;
    int pos_y;
    int vel_x;
    int vel_y;
    int collor;
    
} Ball;

/**
 * Initialize Ball with default values 
*/
Ball Ball_default = {100, 100, 2, 2, 0};

#endif