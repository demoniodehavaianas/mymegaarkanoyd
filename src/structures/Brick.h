#ifndef BRICK_H
#define BRICK_H

#include <sprite_eng.h>

#define MAX_BRICK_ROW 20

typedef struct Brick{
    int pos_x;
    int pos_y;
    int width;
    int height;
    int life;
    int collor;
    Sprite* sprite;
    int name;
} Brick;

Brick Brick_default = {0, 48, 16, 8, 2, 0, NULL, 0};

#endif