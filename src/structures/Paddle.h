/**
 * @file Paddle.h
 * @author Nil Obermüller (demoniodehavaianas@outlook.com.br)
 * @brief Sets the Paddle info
 * @version 0.1
 * @date 2022-01-18
 *
 * @copyright Copyright (c) 2022
 */

#ifndef PADDLE_H
#define PADDLE_H

const int PADDLE_WIDTH = 32; // 32 pixels
const int PADDLE_HEIGHT = 8; // 8 pixels = 1 tile

/**
 * @brief Sets the Paddle's structure
 * \param pos_x
 *      x axys position default is 0
 * \param pos_y
 *      y axys position default is 200
 * \param vel_x
 *      initial velocity default is 0 (only moves in x axys)
 * \param pos_init
 *      initial position at the middle of screen in 144px
 */
typedef struct Paddle
{
  int pos_x;
  int pos_y;
  int vel_x;
  int pos_init;

} Paddle;

Paddle Paddle_default = {0, 200, 0, 144};

#endif
