/**
 * @file Frame.h
 * @author Nil Obermüller (demoniodehavaianas@outlook.com.br)
 * @brief Sets the game boundaries where the player and ball can go
 * @version 0.1
 * @date 2022-01-18
 *
 * @copyright Copyright (c) 2022
 */

const int LEFT_EDGE = 0;
const int RIGHT_EDGE = 320; // max 320px width
const int TOP_EDGE = 0;
const int BOTTOM_EDGE = 224; // max 224px height