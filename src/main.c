/**
 * Meu Mega Arkanoyd
 * Created With Genesis-Code extension for Visual Studio Code
 * Use "Genesis Code: Compile" command to compile this program.
 **/
#include "main.h"
#include <string.h>
#include <music.h>
#include <resources.h>

#include "messages.h"
#include "utilities/utils.h"
#include "structures/Frame.h"
#include "structures/Paddle.h"
#include "structures/Ball.h"
#include "structures/Brick.h"
#include "sound/sound_effects.h"

/* pontuação que define a vitória no jogo */
#define MAX_POINT 25

/* Quantidade de Vidas Iniciais */
const int INITIAL_LIFE = 2;

bool is_game_running = FALSE;
bool is_flashing = FALSE;
int frames = 0;

/* info para a pontuação */
int score = 0;
int highscore = 0;
const int score_pos_y = 2;
int vidas = 0;

Sprite *brick_sprite;
Sprite *ball_sprite;
Sprite *player_sprite;
Paddle paddle;
Ball ball;
Brick* brick;
Brick bricks[MAX_BRICK_ROW];
u16 bricks_left = 0;

void initAndDrawBrick();
void breakBrick();

int main(u16 hard)
{
    resetGameValues();

    drawBackground();

    drawScore();

    drawHiScore();

    drawLife();

    initSFX();

    drawTextInMiddleScreen(MSG_START);

    JOY_setEventHandler(&joyHandle);

    SPR_init(0, 0, 0);

    initBall();

    initPaddle();

    initAndDrawBrick();

    while (1)
    {
        if (is_game_running)
        {
            controlBallMovement();

            controlPlayerMovement();

            flashingEffect();
        }
        // atualiza os sprites
        SPR_update();
        // For versions prior to SGDK 1.60 use VDP_waitVSync instead.
        SYS_doVBlankProcess();
    }
    return (0);
}

void resetGameValues()
{
    score = 0;
    vidas = INITIAL_LIFE;
    ball.vel_x = INITIAL_VELOCITY;
    ball.vel_y = INITIAL_VELOCITY;
}

void drawBackground()
{
    VDP_loadTileSet(bgtile.tileset, 1, DMA);

    VDP_setPalette(PAL1, bgtile.palette->data);
    // desenha uma área com os tiles
    VDP_fillTileMapRect(BG_B, TILE_ATTR_FULL(PAL1, 0, FALSE, FALSE, 1), 0, 0, 40, 30); // 40 = 400 x 30 = 320
}

void drawScore()
{
    VDP_setTextPlane(BG_A);

    VDP_drawText(LABEL_SCORE, label_score_pos_x, label_score_pos_x);

    updateScore();
}

void drawHiScore()
{
    VDP_setTextPlane(BG_A);

    VDP_drawText(MSG_HIGH_SCORE, label_hiscore_pos_x, label_hiscore_pos_y);

    updateHighScore();
}

void drawLife()
{
    VDP_setTextPlane(BG_A);

    VDP_drawText(LABEL_LIFES, label_life_pos_x, label_life_pos_y);

    updateLife();
}

void playMusic()
{ /*
     if (is_game_running)
         XGM_startPlay(xgm_1);
     else
         XGM_stopPlay(xgm_1);
         */
}

void updateScore()
{
    VDP_clearText(1, 2, 3); // limpa o tiles de numeração

    sprintf(str_score, "%d", score);

    // corrigindo a posição da exibição da pontuação
    int span_tile = 6;

    if (strlen(str_score) == 2)
    {
        span_tile = 5;
    }
    else if (strlen(str_score) > 2)
    {
        span_tile = 4;
    }

    VDP_drawText(str_score, span_tile, score_pos_y);

    // aumenta velocidade a cada 10 pontos
    if (score % 10 == 0 && score != 0)
    {
        ball.vel_x += sign(ball.vel_x);
        ball.vel_y += sign(ball.vel_y);
    }

    if (score == 12)
    {
        vidas = vidas + 1;
        updateLife();
    }

    if (MAX_POINT == score)
    {
        endGameWhenWin();
    }
}

void updateHighScore()
{
    VDP_clearText(17, 1, 3);

    sprintf(str_highscore, "%d", highscore);

    VDP_drawText(str_highscore, 20, 1);
}

void updateLife()
{
    VDP_clearText(37, 2, 2);

    sprintf(str_life, "%d", vidas);

    VDP_drawText(str_life, 38, 2); // tile x=38, y=2
}

void drawTextInMiddleScreen(char text[])
{
    KLog(text);

    VDP_drawText(text, 20 - strlen(text) / 2, 15);
}

void initBall()
{
    ball = Ball_default;

    ball_sprite = SPR_addSprite(&spBall, ball.pos_x, ball.pos_y, TILE_ATTR(PAL1, 0, FALSE, FALSE));

    ball.collor = PAL_getColor(22);
}

void initPaddle()
{
    paddle = Paddle_default;

    player_sprite = SPR_addSprite(&spPaddle, paddle.pos_init, paddle.pos_y, TILE_ATTR(PAL1, 0, FALSE, FALSE));
}

void initAndDrawBrick() {

    brick = bricks;

    for(int i = 0; i <= MAX_BRICK_ROW; i++)
    {
        brick->width = 16;
        brick->height = 8;
        brick->pos_x = (i == 0) ? 0 : brick->pos_x + brick->width;
        brick->pos_y = 48;
        brick->life = 2;
        brick->collor = PAL_getColor(1);
        brick->name = i;

        brick->sprite = SPR_addSprite(&spBrickG, brick->pos_x, brick->pos_y, TILE_ATTR(PAL2, 0, FALSE, FALSE));
    }
}

void breakBreak(Brick* anotherBrick){

    anotherBrick->life = anotherBrick->life - 1;
    
    if(anotherBrick->life > 0 && anotherBrick->life < 2) 
    {
        // implementar acrregar o tijolo quebradoi
    } else {
        SPR_setVisibility(anotherBrick->sprite, HIDDEN);
    }
}

void verifyBallEdgeColision()
{
    // verifica as margens horizontais
    if (ball.pos_x < LEFT_EDGE)
    {
        playHitWallSFX();

        ball.pos_x = LEFT_EDGE;
        ball.vel_x = -ball.vel_x;
    }
    else if (ball.pos_x + BALL_WIDTH > RIGHT_EDGE)
    {
        playHitWallSFX();

        ball.pos_x = RIGHT_EDGE - BALL_WIDTH;
        ball.vel_x = -ball.vel_x;
    }

    // verifica as margens verticais
    if (ball.pos_y < TOP_EDGE)
    {
        playHitWallSFX();

        ball.pos_y = TOP_EDGE;
        ball.vel_y = -ball.vel_y;
    }
    else if (ball.pos_y + BALL_HEIGHT > BOTTOM_EDGE)
    {
        endGame();
    }
}

void controlBallMovement()
{
    verifyBallEdgeColision();

    if (ball.pos_x < paddle.pos_x + PADDLE_WIDTH && // se a bola encostou na parte dir do paddle
        ball.pos_x + BALL_WIDTH > paddle.pos_x)     // se a bola encostou na parte esq do paddle
    {
        // se a bola encostou na parte de cima do paddle
        if (ball.pos_y < paddle.pos_y + PADDLE_HEIGHT &&
            ball.pos_y + BALL_HEIGHT >= paddle.pos_y)
        {
            // se a bola tocou na parte esquerda do paddle ...
            if (ball.pos_x + BALL_WIDTH < paddle.pos_x + PADDLE_WIDTH)
            {
                KLog("acertou o lado ESQUERDO do paddle");
                // verifica a orientação da bola ...
                if (ball.vel_x > 0)
                {
                    KLog("bola veio da esq -> dir");
                    ball.vel_x = -ball.vel_x;
                    // ball.pos_x = paddle_1.pos_x - BALL_WIDTH - 1;
                }
            } // se a bola tocou a parte direita do paddle...
            else if (ball.pos_x + BALL_WIDTH > paddle.pos_x + PADDLE_WIDTH)
            {
                KLog("acertou o lado DIREITO do paddle");
                if (ball.vel_x < 0)
                {
                    KLog("bola veio da dir -> esq");
                    ball.vel_x = -ball.vel_x;
                    // ball.pos_x = paddle_1.pos_x + paddle_1.width - BALL_WIDTH - 1;
                }
            }
            // diminui 1px da bola pra evitar ficar preso no paddle
            ball.pos_y = paddle.pos_y - BALL_HEIGHT - 1;
            // inverte a velocidade para ir na direção oposta
            ball.vel_y = -ball.vel_y;

            // aumenta a pontuação e atualiza o HUD
            score++;
            // indica que a bola deve piscar
            is_flashing = TRUE;

            playBallPaddleSFX();

            updateScore();
        }
    }
    // posição da bola
    ball.pos_x += ball.vel_x;
    ball.pos_y += ball.vel_y;

    SPR_setPosition(ball_sprite, ball.pos_x, ball.pos_y);
}

void controlPlayerMovement()
{
    paddle.pos_x += paddle.vel_x;

    if (paddle.pos_x < LEFT_EDGE)
        paddle.pos_x = LEFT_EDGE;

    if (paddle.pos_x + PADDLE_WIDTH > RIGHT_EDGE)
        paddle.pos_x = RIGHT_EDGE - PADDLE_WIDTH;

    SPR_setPosition(player_sprite, paddle.pos_x, paddle.pos_y);
}

void flashingEffect()
{
    if (is_flashing)
    {
        frames++;

        if (frames % 4 == 0)
        {
            VDP_setPaletteColor(22, ball.collor);
        }
        else if (frames % 2 == 0)
        {
            VDP_setPaletteColor(22, RGB24_TO_VDPCOLOR(0xffffff));
        }

        if (frames > 30) // meio segundo
        {
            is_flashing = FALSE;
            frames = 0;
            VDP_setPaletteColor(22, ball.collor);
        }
    }
}

void joyHandle(u16 joy, u16 changed, u16 state)
{
    if (joy == JOY_1)
    {
        if (state & BUTTON_START)
        {
            if (!is_game_running)
            {
                initGame();
            }
        }

        if (state & BUTTON_RIGHT)
        {
            paddle.vel_x = 3;
        }
        else if (state & BUTTON_LEFT)
        {
            paddle.vel_x = -3;
        }
        else if ((changed & BUTTON_RIGHT) | (changed & BUTTON_LEFT))
        {
            paddle.vel_x = 0;
        }
    }
}

void initGame()
{
    updateScore();

    updateLife();

    updateHighScore();

    //(random() % (MAX – MIN + 1)) + MIN;
    int posix_aleatoria = (random() % (RIGHT_EDGE - LEFT_EDGE + 1)) + LEFT_EDGE;

    // ball.pos_x = posix_aleatoria; // a bola começa numa posição x aleatória
    ball.pos_y = 0;

    ball.vel_x = posix_aleatoria > (RIGHT_EDGE / 2) ? -1 : 1; // bola vai pra esq ou dir aleatóriamente

    // coloca o jogador novamente no meio da tela
    paddle.pos_x = paddle.pos_init;

    // limpa o texto da tela
    VDP_clearTextArea(0, 10, 40, 10);

    playMusic();

    is_game_running = TRUE;
}

void endGame()
{
    KLog_F1("Perdeu uma vida ", vidas);

    if (is_game_running)
    {
        vidas = vidas - 1;

        playBallDieSFX();

        is_game_running = FALSE;

        if (vidas < 0)
        {

            drawTextInMiddleScreen(MSG_GAME_OVER);

            vidas = INITIAL_LIFE;

            if (highscore < score)
                highscore = score;

            resetGameValues();
        }
        else
        {

            initGame();
        }
    }
}

void endGameWhenWin()
{
    if (is_game_running)
    {
        is_game_running = FALSE;

        victoySFX();

        if (highscore < score)
        {
            highscore = score;
        }
    }

    updateHighScore();

    drawTextInMiddleScreen(MSG_VITORIA);
}