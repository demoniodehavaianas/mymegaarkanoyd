#ifndef UTILS_H
#define UTILS_H

/**
 * @brief verifica o sinal 
 * 
 * @param x valor de x
 * @return int inteiro
 */
int sign(int x);

#endif