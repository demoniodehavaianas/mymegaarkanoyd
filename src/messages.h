/**
 * @file messages.h
 * @author Nil Obermüller (demoniodehavaianas@outlook.com.br)
 * @brief All labels and messages in game are here
 * @version 0.1
 * @date 2022-01-18
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef MESSAGES_H
#define MESSAGES_H

char MSG_START[33] = "PRESSIONE START PARA COMECAR!\0";
char MSG_GAME_OVER[11] = "GAME OVER\0";
char MSG_VITORIA[10] = "VITORIA!\0";
char MSG_HIGH_SCORE[10] = "HI-SCORE\0";
char LABEL_SCORE[8] = "PONTOS\0";
char LABEL_LIFES[7] = "VIDAS\0";

int label_hiscore_pos_x = 16;
int label_hiscore_pos_y = 0;

int label_score_pos_x = 1;
int label_score_pos_y = 1;

int label_life_pos_x = 34;
int label_life_pos_y = 1;

char str_highscore[4] = "0";
char str_score[4] = "0";
char str_life[2] = "0";

/**
 * @brief Draw the Score's label
 */
void drawScore();

/**
 * @brief Draw the HI-SCORE's label
 *
 */
void drawHiScore();

/**
 * @brief Draw the life's label
 *
 */
void drawLife();

/**
 * @brief update Score
 */
void updateScore();

/**
 * @brief update Hi-Score
 */
void updateHighScore();

/**
 * @brief update Life
 */
void updateLife();

#endif