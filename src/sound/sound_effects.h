/**
 * @file sound_effects.h
 * @author Nil Obermüller (demoniodehavaianas@outlook.com.br)
 * @brief SFX definitions file
 * @version 0.1
 * @date 2022-01-18
 * 
 * @copyright Copyright (c) 2022
 */

#ifndef SOUND_EFFECTS_H //INCLUDE GUARDS
#define SOUND_EFFECTS_H

#include <genesis.h>
#include <sfx.h>

/* sfx sample IDs */
#define SFX_BALL_HIT 64
#define SFX_BALL_HIT_WALL 65
#define SFX_BALL_DIE 66
#define SFX_VICTORY 67

/**
 * inicializa os efeitos sonoros.
 * Aqui deve conter a inicialização de todos os objetos de efeitos sonoros.
 */
/**
 * @brief initialize all the sfx using in game.
 * All sfx defined in 'sfx.res' must start here.
 * 
 */
void initSFX();

/**
 * @brief play the sfx when ball hits the wall
  */
void playHitWallSFX();

/**
 * @brief play the sfx when ball hits paddle
  */
void playBallPaddleSFX();

/**
 * @brief play the sfx when ball fall into the pit
 */
void playBallDieSFX();

/**
 * @brief play the sfx when the player wins
 *
 */
void victoySFX();

#endif