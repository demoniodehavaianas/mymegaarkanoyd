#include "sound_effects.h"

void initSFX()
{
    XGM_setPCM(SFX_BALL_HIT, sfx_ball_hit, sizeof(sfx_ball_hit));
    XGM_setPCM(SFX_BALL_HIT_WALL, sfx_ball_hit_wall, sizeof(sfx_ball_hit_wall));
    XGM_setPCM(SFX_BALL_DIE, sfx_ball_die, sizeof(sfx_ball_die));
    XGM_setPCM(SFX_VICTORY, sfx_victory, sizeof(sfx_victory));
}

void playHitWallSFX()
{
    SND_startPlayPCM_XGM(SFX_BALL_HIT_WALL, 1, SOUND_PCM_CH2);
}

void playBallPaddleSFX()
{
    SND_startPlayPCM_XGM(SFX_BALL_HIT, 1, SOUND_PCM_CH2);
}

void playBallDieSFX()
{
    SND_startPlayPCM_XGM(SFX_BALL_DIE, 1, SOUND_PCM_CH2);
}

void victoySFX()
{
    SND_startPlayPCM_XGM(SFX_VICTORY, 1, SOUND_PCM_CH3);
}
