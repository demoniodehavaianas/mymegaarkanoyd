/**
 * @file main.h
 * @author Nil Obermüller (demoniodehavaianas@outlook.com.br)
 * @brief 
 * @version 0.1
 * @date 2022-01-18
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include <genesis.h>

/**
 * @brief reset score, lifes and ball's velocity
  */
void resetGameValues();

/**
 * @brief draw background with repeated tiles
 */
void drawBackground();

/**
 * @brief play music
  * (disabled, music is loud and noised)
 */
void playMusic();

/**
 * @brief Draw a text in the Middle of Screen
 *
 * @param text
 */
void drawTextInMiddleScreen(char text[]);

/**
 * @brief initialize ball's sprite and define its collor palette
 */
void initBall();

/**
 * @brief initialize the 'paddle'
 */
void initPaddle();

/**
 * @brief verify if ball hits the edge and do what it needs
 */
void verifyBallEdgeColision();

/**
 * @brief create ball movement
 */
void controlBallMovement();

/**
 * @brief player movement
 */
void controlPlayerMovement();

/**
 * @brief flashing effect when ball hits the paddle
 */
void flashingEffect();

void joyHandle(u16 joy, u16 changed, u16 state);

/**
 * @brief initialize new Game
 *
 */
void initGame();

/**
 * @brief when 'bites the dust' is game over
 *
 */
void endGame();

/**
 * @brief when player wins
 *
 */
void endGameWhenWin();